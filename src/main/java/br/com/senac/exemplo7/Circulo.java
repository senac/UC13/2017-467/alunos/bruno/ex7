
package br.com.senac.exemplo7;

public class Circulo extends figurasGeometricas{

    private double raio;

    public Circulo() {
    }

    public Circulo(double raio) {
        this.raio = raio;
    }
    
    @Override
    public double getAreaTotal() {
       
        return (this.raio * this.raio)* 3.14;
    } 
}
