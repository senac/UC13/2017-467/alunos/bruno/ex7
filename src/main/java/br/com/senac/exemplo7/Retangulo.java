
package br.com.senac.exemplo7;

public class Retangulo extends figurasGeometricas{

    private double base;
    private double altura;
    
    

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public Retangulo() {
    }

    @Override
    public double getAreaTotal() {
         return this.altura * this.base;
    }
   
}
