
package br.com.senac.exemplo7;


public class Triangulo extends figurasGeometricas {

    private double base;
    private double altura;

    public Triangulo() {
    }

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }
    
    
    @Override
    public double getAreaTotal() {
        return (this.altura * this.base) / 2;
    }
   
}
