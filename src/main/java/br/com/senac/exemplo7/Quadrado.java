/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exemplo7;

import java.awt.geom.Area;

public class Quadrado extends figurasGeometricas {

    private double lado1 ; 
    private double lado2 ; 

    public Quadrado() {
    }

    public Quadrado(double lado1, double lado2) {
        this.lado1 = lado1;
        this.lado2 = lado2;
    }
    
    @Override
    public double getAreaTotal() {
        
       return this.lado1 * this.lado2;
         
    }
}
