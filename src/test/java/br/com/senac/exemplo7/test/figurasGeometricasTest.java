/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exemplo7.test;

import br.com.senac.exemplo7.Circulo;
import br.com.senac.exemplo7.Quadrado;
import br.com.senac.exemplo7.Retangulo;
import br.com.senac.exemplo7.Triangulo;
import org.junit.Test;
import static org.junit.Assert.*;

public class figurasGeometricasTest {
    
    public figurasGeometricasTest() {
        
    }
    
    @Test
    public void AreaTotalQuadrado(){
       Quadrado quadrado = new Quadrado(10,10);
       
        assertEquals(100 ,quadrado.getAreaTotal(), 0.01);
        
    }
    @Test
    public void AreaTotalRetangulo(){
     Retangulo retangulo = new Retangulo(10,10);
       
        assertEquals(100 ,retangulo.getAreaTotal(), 0.01);
        
    }
    @Test
    public void AreaTotalTriangulo(){
        Triangulo triangulo = new Triangulo(10 ,20);
       
        assertEquals(100 ,triangulo.getAreaTotal(), 0.01);
        
    }
    @Test
    public void AreaTotalCirculo(){
        Circulo circulo = new Circulo(10);
       
        assertEquals(314 ,circulo.getAreaTotal(), 0.01);
        
    }
    
}